import socket
import threading
import time

PORT = 65432
SERVER = "127.0.0.1"
ADDR = (SERVER, PORT)
FORMAT = "utf-8"

currentPlayer = "one"
server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server.bind(ADDR)
clients = []
acceptedMoves = ['P', 'R', 'S']
moves = []
scorePlayer1 = 0
scorePlayer2 = 0

def verifyPlay(moves):
  global scorePlayer1
  global scorePlayer2

  winner = ""
  if len(moves) != 2:
    print('Both players must play to verify who won')
    return "Invalid play"

  if moves[0].upper() == "P":
    if moves[1].upper() == "P":
      winner = "Draw"
    elif moves[1].upper() == "R":
      winner = "Player #1 won"
      scorePlayer1 = scorePlayer1 + 1
    elif moves[1].upper() == "S":
      winner = "Player #2 won"
      scorePlayer2 = scorePlayer2 + 1
  elif moves[0].upper() == "R":
    if moves[1].upper() == "R":
      winner = "Draw"
    elif moves[1].upper() == "S":
      winner = "Player #1 won"
      scorePlayer1 = scorePlayer1 + 1
    elif moves[1].upper() == "P":
      winner = "Player #2 won"
      scorePlayer2 = scorePlayer2 + 1
  elif moves[0].upper() == "S":
    if moves[1].upper() == "S":
      winner = "Draw"
    elif moves[1].upper() == "P":
      scorePlayer1 = scorePlayer1 + 1
      winner = "Player #1 won"
    elif moves[1].upper() == "R":
      winner = "Player #2 won"
      scorePlayer2 = scorePlayer2 + 1
  
  return winner


def client(conn, addr):
  global scorePlayer1
  global scorePlayer2
  global currentPlayer
  global moves

  print(f"{addr} CONNECTED!! \n")
  host, port = addr
  if len(clients) < 2:
    if not port in clients:
      clients.append(port)
  else:
    print('max clients reached')

  connected = True
  while connected:
    score = f"{scorePlayer1}_{scorePlayer2}"
    msg = conn.recv(1024).decode(FORMAT)
    # if the client exists
    if port in clients:
      # verify if the message is comming from player #1
      if port == clients[0]:
        # if the current player is the player #1, prints a message and
        # change the current player for player #2
        if currentPlayer == "one":
          if not msg.upper() in acceptedMoves:
            conn.send(bytes(f"{score}-Invalid move. You must play (R) Rock, (P) Paper or (S) Scissor", encoding=FORMAT))
            continue
          else:
            if len(moves) < 2:
              moves.append(msg)
            conn.send(bytes(f"{score}-waiting for player #2", encoding=FORMAT))
            currentPlayer = "two"
        else:
          # if it's not the turn of player #1, prints a message
          conn.send(bytes(f"{score}-It's the turn of player #2", encoding=FORMAT))
      # verify if the message is coming from player #1
      elif port == clients[1]:
        # if the current player is the player #2, prints a message and
        # change the current player for player #1
        if currentPlayer == "two":
          if not msg.upper() in acceptedMoves:
            conn.send(bytes(f"{score}-Invalid move. You must play (R) Rock, (P) Paper or (S) Scissor", encoding=FORMAT))
            continue
          else:
            if len(moves) < 2:
              moves.append(msg)
            conn.send(bytes(f"{score}-waiting for player #1", encoding=FORMAT))
            currentPlayer = "one"
        else:
          # print("It's the turn of player #1")
          # if msg:
          conn.send(bytes(f"{score}-It's the turn of player #1", encoding=FORMAT))

    if len(moves) == 2:
      print(verifyPlay(moves))
      moves = []

    if msg:
      if msg == "quit":
        connected = False
        conn.close() 
      # print(f"[{port}] SAYS : {msg}")

def start():
  server.listen()
  print("< RUNNING > WAITING FOR CONNECTIONS ....")
  while True:
    conn, addr = server.accept()
    thread = threading.Thread(target=client, args=(conn, addr))
    thread.start()

print("< STARTING > STARTING THE SERVER ....")
time.sleep(1)
start()