import socket
import os

PORT = 65432
SERVER = "127.0.0.1"
ADDR = (SERVER, PORT)
FORMAT = "utf-8"
client = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
client.connect(ADDR)

scoreP1 = 0
scoreP2 = 0

def gameMenu():
  global scoreP1
  global scoreP2

  p1 = int(scoreP1)
  p2 = int(scoreP2)

  print("\n")
  print(f"   Player 1 - {p1} x {p2} Player 2")
  print(" ============================= ")
  print(" NOW THE GAMEPLAY STARTS \ O / ")
  print(" ============================= ")
  print("\n")
  print("[ R ] FOR ROCK ✊ ")
  print("[ P ] FOR PAPER ✋ ")
  print("[ S ] FOR SCISSOR 🤞")

def clearTerminal():
  os.system('cls' if os.name == 'nt' else 'clear')

def send(msg):
  global scoreP1
  global scoreP2

  client.send(bytes(msg, encoding='utf-8'))
  response = client.recv(1024).decode(FORMAT)
  data = response.split("-")
  scores = data[0].split("_")
  scoreP1 = scores[0]
  scoreP2 = scores[1]
  clearTerminal()
  print(data[1])
  print(f"score: {scores}")
  # if response == "!clear":
  #   print("It's not your turn")

while True:
  gameMenu()
  print("\nYoo, Rock, Paper or Scissors??: ", end="")
  msg = input()
  if msg == 'quit':
    break
  send(msg)